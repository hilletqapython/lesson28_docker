import psycopg2

conn = psycopg2.connect(
    host="localhost",
    database="mydb",
    user="postgres",
    password="mysecretpassword"
)

cur = conn.cursor()

cur.execute("SELECT * FROM mytable")
rows = cur.fetchall()

for row in rows:
    print(row)

cur.close()
conn.close()
